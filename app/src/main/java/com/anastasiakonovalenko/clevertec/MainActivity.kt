package com.anastasiakonovalenko.clevertec

import android.os.Build
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val textView: TextView = findViewById(R.id.helloWorld)
        if (Build.VERSION.SDK_INT > 24) {
            textView.setTextColor(getColorStateList(R.color.textViewRedColor))
        }
    }
}